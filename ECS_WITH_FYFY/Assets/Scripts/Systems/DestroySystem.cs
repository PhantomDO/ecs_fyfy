using UnityEngine;
using FYFY;

public class DestroySystem : FSystem {

    private Family _destroyGO = FamilyManager.getFamily(new AllOfComponents(typeof(MarkForDestroy)));

	public DestroySystem()
	{
        _destroyGO.addEntryCallback(onMarkForDestroy);
	}

    private void onMarkForDestroy(GameObject go)
    {
        if (go.TryGetComponent(out MarkForDestroy mfd) && mfd.onDestroy != null)
        {
            mfd.onDestroy?.Invoke();
        }

        GameObjectManager.unbind(go);
        Object.Destroy(go);
    }
}