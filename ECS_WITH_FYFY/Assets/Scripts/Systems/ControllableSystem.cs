using UnityEngine;
using FYFY;

public class ControllableSystem : FSystem
{

    private Family _controllableGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(Move)), 
        new NoneOfComponents(typeof(RandomTarget)));
	
	// Use to process your families.
	protected override void onProcess(int familiesUpdateCount)
    {
        foreach (var go in _controllableGO)
        {
            var current = go.transform.position;
            var speed = go.GetComponent<Move>().speed;
            go.transform.position = Vector3.MoveTowards(current, InputMovement(current), speed * Time.deltaTime);
        }

    }

    private Vector3 InputMovement(Vector3 currentPosition)
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"), 0);
        return currentPosition + movement;
    }
}