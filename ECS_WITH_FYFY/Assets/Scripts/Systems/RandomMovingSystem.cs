using UnityEngine;
using FYFY;

public class RandomMovingSystem : FSystem
{

    private Family _randomMovingGO = FamilyManager.getFamily(new AllOfComponents(typeof(Move), typeof(RandomTarget)));

	public RandomMovingSystem()
	{
        foreach (var go in _randomMovingGO)
        {
            onGoEnter(go);
        }

		_randomMovingGO.addEntryCallback(onGoEnter);
	}

    private void onGoEnter(GameObject go)
    {
        if (go.TryGetComponent(out RandomTarget rt))
        {
            rt.target = go.transform.position;
        }
    }

	// Use to process your families.
	protected override void onProcess(int familiesUpdateCount) {

        foreach (var go in _randomMovingGO)
        {
            if (!go.TryGetComponent(out RandomTarget rt)) continue;

            if (Vector3.Distance(go.transform.position, rt.target) <= rt.reachingDistance)
            {
                rt.target = new Vector3((Random.value - 0.5f) * 7, (Random.value - 0.5f) * 5.2f);
            }
            else
            {
                var current = go.transform.position;
                var speed = go.GetComponent<Move>().speed;
                go.transform.position = Vector3.MoveTowards(current, rt.target, speed * Time.deltaTime);
            }
        }

	}
}