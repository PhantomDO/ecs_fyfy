using UnityEngine;
using FYFY;

public class ItemFactory : FSystem
{
    private Family factory_F = FamilyManager.getFamily(new AllOfComponents(typeof(Factory)));

    public ItemFactory()
    {
        foreach (var go in factory_F)
        {
            if (!go.TryGetComponent(out Factory factory) && 
                factory.dyingPrefab == null) continue;

            factory_F.addExitCallback(onFactoryEntityDie);
        }
    }

    private void onFactoryEntityDie(int id)
    {
        Debug.Log($"Entity[{id}] drop item when dying.");
    }

	// Use to process your families.
	protected override void onProcess(int familiesUpdateCount)
    {
        foreach (var go in factory_F)
        {
            if (!go.TryGetComponent(out Factory factory)) continue;
            if (factory.alivePrefab == null) continue;

            factory.reloadProgress += Time.deltaTime;

            if (factory.reloadProgress >= factory.reloadTime)
            {
                createItem(factory, false);
                factory.reloadProgress = 0;
            }
        }
        
    }

    public GameObject[] createItem(Factory factory, bool onDestroy = false)
    {
        if (factory.count <= 0) return null;

        GameObject[] gos = new GameObject[factory.count];
        for (int i = 0; i < factory.count; i++)
        {
            gos[i] = Object.Instantiate<GameObject>(
                onDestroy ? factory.dyingPrefab : factory.alivePrefab);
            GameObjectManager.bind(gos[i]);

            var rdmCircle = Random.insideUnitCircle * factory.count * factory.nearOffset;
            
            Vector3 randomPosition = onDestroy
                ? factory.gameObject.transform.position + new Vector3(rdmCircle.x, rdmCircle.y, 0)
                : new Vector3((Random.value - 0.5f) * 7, (Random.value - 0.5f) * 5.2f);

            gos[i].transform.position = randomPosition;
        }

        return gos;
    }

    public void popVirus(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            var go = factory_F.First();
            if (go.TryGetComponent(out Factory factory))
            {
                createItem(factory);
            }
        }
    }
}