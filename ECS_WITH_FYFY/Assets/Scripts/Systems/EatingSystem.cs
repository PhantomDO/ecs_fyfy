using UnityEngine;
using FYFY;
using FYFY_plugins.TriggerManager;

public class EatingSystem : FSystem
{   
    private Family _triggeredGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(Triggered2D), typeof(Eating)));
    
    // Use to process your families.
	protected override void onProcess(int familiesUpdateCount) {
        Debug.Log($"Trigger Family contains {_triggeredGO.Count} member");
        foreach (var go in _triggeredGO)
        {
            if (!go.TryGetComponent(out Triggered2D trigger2D)) continue;

            foreach (var target in trigger2D.Targets)
            {
                AddEatenTag(target);
                if (go.TryGetComponent(out Eating eating))
                {
                    eating.alreadyEaten++;

                    // Destroy eater when it is full
                    if (eating.alreadyEaten >= eating.capacity)
                    {
                        GameObjectManager.unbind(go);
                        Object.Destroy(go);
                    }
                }
            }
        }
	}

    private void AddEatenTag(GameObject go)
    {
        EatenAction eatenAction = EatenAction.Destroy;

        if (go.CompareTag("Virus") || go.CompareTag("Bacterie") || go.CompareTag("Toxine"))
        {
            eatenAction = EatenAction.Destroy;
        }
        
        if (go.CompareTag("Cell"))
        {

        }

        GameObjectManager.addComponent<Eaten>(go, new { action = eatenAction });
    }
}