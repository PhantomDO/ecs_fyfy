using UnityEngine;
using FYFY;
using UnityEngine.Events;

[System.Serializable]
public enum EatenAction
{
    Destroy,
    Damage,

}

public class EatenSystem : FSystem {

    private Family _eatenGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(Eaten)));

    public EatenSystem()
    {
        _eatenGO.addEntryCallback(onBeingEaten);
        _eatenGO.addExitCallback(onHaveBeenEaten);
    }

    private void onBeingEaten(GameObject go)
    {
        if (!go.TryGetComponent(out Eaten eated)) return;

        switch (eated.action)
        {
            case EatenAction.Destroy:
                GameObjectManager.addComponent<MarkForDestroy>(go);
                break;
            case EatenAction.Damage:
                break;
            default:
                throw new System.ArgumentOutOfRangeException();
        }

        if (eated.action != EatenAction.Destroy) 
            GameObjectManager.removeComponent<Eaten>(go);
    }

    private void onHaveBeenEaten(int id)
    {

    }
}