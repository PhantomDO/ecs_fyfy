using UnityEngine;

public class Eating : MonoBehaviour {
	// Advice: FYFY component aims to contain only public members (according to Entity-Component-System paradigm).
    [HideInInspector] public uint alreadyEaten;
    public uint capacity = 5;
}