using System;
using UnityEngine;
using UnityEngine.Events;

public class MarkForDestroy : MonoBehaviour
{
    public UnityEvent onDestroy = null;
}