using System;
using UnityEngine;
using UnityEngine.Serialization;

public class Eaten : MonoBehaviour {
	// Advice: FYFY component aims to contain only public members (according to Entity-Component-System paradigm).
    public EatenAction action;
}