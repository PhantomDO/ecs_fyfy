using System;
using UnityEngine;
using UnityEngine.Serialization;

public class Factory : MonoBehaviour {
	// Advice: FYFY component aims to contain only public members (according to Entity-Component-System paradigm).
    [Header("Update")] 
    public float reloadTime = 2f;
    public float reloadProgress = 0f;
    public GameObject alivePrefab;

    [Header("OnDestroy")]
    public uint count = 1;
    public float nearOffset = 0.08f;
    public GameObject dyingPrefab;
}