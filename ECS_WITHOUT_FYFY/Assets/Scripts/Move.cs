using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[DisallowMultipleComponent]
public class Move : MonoBehaviour
{
    public float speed = 2.5f;
    public bool isPlayable = false;
    public BoxCollider constraints;

    public float lenghtSphere = 10f;
    public float reachingDistance = 0.02f;
    private Vector3 _destination = Vector3.zero;
    private Vector3 _nextMovement = Vector3.zero;
    
    void Update()
    {
        if (isPlayable)
        {
            _nextMovement = InputMovement();
        }
        else
        {
            if (Vector3.Distance(_nextMovement, transform.position) <= reachingDistance)
            {
                _destination = Random.insideUnitSphere * lenghtSphere;
                _destination.z = 0;
                _nextMovement = RandomMovement(_destination);
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, 
            ConstrainInsideBounds(_nextMovement), 
            speed * Time.deltaTime);
    }

    private Vector3 InputMovement()
    {
        Vector3 movement = transform.position;
        Vector3 addPosition = new Vector3(Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"), 0);
        return movement + addPosition;
    }

    private Vector3 RandomMovement(Vector3 destination)
    {
        Vector3 movement = transform.position;
        return movement + destination.normalized;
    }

    private Vector3 ConstrainInsideBounds(Vector3 movement)
    {
        Vector3 constrainMovement = movement;

        if (constraints)
        {
            var bounds = constraints.bounds;
            constrainMovement.x = Mathf.Clamp(constrainMovement.x, bounds.min.x, bounds.max.x);
            constrainMovement.y = Mathf.Clamp(constrainMovement.y, bounds.min.y, bounds.max.y);
            constrainMovement.z = Mathf.Clamp(constrainMovement.z, bounds.min.z, bounds.max.z);
        }

        return constrainMovement;
    }
}
