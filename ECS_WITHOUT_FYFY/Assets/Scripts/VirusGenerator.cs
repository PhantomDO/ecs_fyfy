using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class VirusGenerator : MonoBehaviour
{
    public GameObject virusPrefab;
    public BoxCollider constraints;

    public float reloadTime = 2f;
    private float _reloadProgress = 0f;
    
    void Update()
    {
        _reloadProgress += Time.deltaTime;
        if (_reloadProgress >= reloadTime)
        {
            var rdmPosition = Random.insideUnitSphere;
            rdmPosition.z = 0;
            
            if (constraints)
            {
                var bounds = constraints.bounds;
                rdmPosition.x *= bounds.extents.x;
                rdmPosition.y *= bounds.extents.y;
            }

            var instance = Instantiate(virusPrefab, ConstrainInsideBounds(rdmPosition), Quaternion.identity);
            if (instance.TryGetComponent(out Move move) && constraints)
            {
                move.constraints = constraints;
            }
            _reloadProgress = 0f;
        }
    }


    private Vector3 ConstrainInsideBounds(Vector3 movement)
    {
        Vector3 constrain = movement;

        if (constraints)
        {
            var bounds = constraints.bounds;
            constrain.x = Mathf.Clamp(constrain.x, bounds.min.x, bounds.max.x);
            constrain.y = Mathf.Clamp(constrain.y, bounds.min.y, bounds.max.y);
            constrain.z = Mathf.Clamp(constrain.z, bounds.min.z, bounds.max.z);
        }

        return constrain;
    }
}
